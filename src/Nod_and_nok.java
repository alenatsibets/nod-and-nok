import java.util.Scanner;

public class Nod_and_nok {
    public static void main(String[] args) {
        int x,y, nod, nok;
        x = inputIntFromConsole("Enter the first value: ");
        y = inputIntFromConsole("Enter the second value: ");
        nod = nod(x, y);
        nok = nok(x, y, nod);
        display("The greatest common divisor: ", nod);
        display("The smallest common multiple: ", nok);
    }
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Incorrect value. " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int nod(int x, int y) {
        while (x != 0 && y != 0) {
            if (x > y) { x = x % y; }
            else { y = y % x; }
        }
        return x + y;
    }
    public static int nok(int x, int y, int nod) {
        return x*y/nod;
    }
    public static void display(String message, int x){
        System.out.println(message + x);
    }
}